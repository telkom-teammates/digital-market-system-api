# Study Case Chapter 1 TEFA 2023

Kelompok 4 (padiumkm.id)

| NRP | Nama | Perguran Tinggi |
| ------ | ------ | ------ |
|    5025201003    |    Rahmat Faris Akbar |ITS|
|    5025201131    |    Mohammad Fadhil Rasyidin Parinduri| ITS |

---

- [x] Login & register with auth features
- [x] Able to show List of products
- [x] Shopping cart
- [x] Track orders
- [x] Display time notations (Big O)
- [x] [Report (Click Here!)](https://docs.google.com/document/d/1ykB5jj1rGvaDyyBverHyKzcgkf7SwzbDkqjqZEGw1Q4/edit?usp=sharing)

## User Guide
Start the project with below commands
```bash
docker-compose up -d
go run main.go
```

- **SIGNUP FUNCTION API CALL (POST REQUEST)**

http://localhost:8000/users/signup

```json
{
  "first_name": "Rahmat3",
  "last_name": "Akbar3",
  "email": "rfa3@gmail.com",
  "password": "123456",
  "phone": "+62345678903"
}
```

Response :"Successfully Signed Up!!"

- **LOGIN FUNCTION API CALL (POST REQUEST)**

  http://localhost:8000/users/login

```json
{
  "email": "rfa3@gmail.com",
  "password": "123456"
} 
```

response will be like this

```json
{
    "_id": "6457b1bcd1624752fc80d1a1",
    "first_name": "Rahmat3",
    "last_name": "Akbar3",
    "password": "$2a$14$j1UplsOKjXpBmJDOkoRMFua2I64rpnDWSAads1j53FuhgJpF16JbS",
    "email": "rfa3@gmail.com",
    "phone": "+62345678903",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbWFpbCI6InJmYTNAZ21haWwuY29tIiwiRmlyc3RfTmFtZSI6IlJhaG1hdDMiLCJMYXN0X05hbWUiOiJBa2JhcjMiLCJVaWQiOiI2NDU3YjFiY2QxNjI0NzUyZmM4MGQxYTEiLCJleHAiOjE2ODM1NTUxODN9.yMdRKRd4JRlAk3lovyPVjvaYM1iFMeOeJzP9DsxI8w0",
    "Refresh_Token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbWFpbCI6IiIsIkZpcnN0X05hbWUiOiIiLCJMYXN0X05hbWUiOiIiLCJVaWQiOiIiLCJleHAiOjE2ODQwNzM1ODN9.FFHSOMuINcQet1RvfLzgSjJyu5ybcintKaCg_AvRTnw",
    "created_at": "2023-05-07T14:12:12Z",
    "updtaed_at": "2023-05-07T14:12:12Z",
    "user_id": "6457b1bcd1624752fc80d1a1",
    "usercart": [],
    "address": [],
    "orders": []
}
```

- **Admin add Product Function (POST REQUEST)**

  http://localhost:8000/admin/addproduct

```json
{
  "product_name": "Hampers Wellness 3 by UMKM Mitra Sarinah",
  "price": 115000,
  "rating": 10,
  "image": "hampers_wellness_3.jpg"
}
```

Response : "Successfully added our Product Admin!!"

- **View all the Products in db GET REQUEST**

  http://localhost:8000/users/productview

Response

```json
[
    {
        "Product_ID": "64577070173f3b3983fd5ae1",
        "product_name": "Natural Soap",
        "price": 89000,
        "rating": 10,
        "image": "natural_soap.jpg"
    },
    {
        "Product_ID": "6457771d173f3b3983fd5ae2",
        "product_name": "Hampers Wellness 2 by UMKM Mitra Sarinah",
        "price": 114000,
        "rating": 10,
        "image": "hampers_wellness.jpg"
    },
    {
        "Product_ID": "6457b222d1624752fc80d1a2",
        "product_name": "Hampers Wellness 3 by UMKM Mitra Sarinah",
        "price": 115000,
        "rating": 10,
        "image": "hampers_wellness_3.jpg"
    }
]
```

- **Search Product by regex function (GET REQUEST)**

defines the word search sorting
http://localhost:8000/users/search?name=Ha

response:

```json
[
    {
        "Product_ID": "6457771d173f3b3983fd5ae2",
        "product_name": "Hampers Wellness 2 by UMKM Mitra Sarinah",
        "price": 114000,
        "rating": 10,
        "image": "hampers_wellness.jpg"
    },
    {
        "Product_ID": "6457b222d1624752fc80d1a2",
        "product_name": "Hampers Wellness 3 by UMKM Mitra Sarinah",
        "price": 115000,
        "rating": 10,
        "image": "hampers_wellness_3.jpg"
    }
]
```

- **Adding the Products to the Cart (GET REQUEST)**

  http://localhost:8000/addtocart?id=xxxproduct_idxxx&userID=xxxxxxuser_idxxxxxx

  Corresponding mongodb query

- **Removing Item From the Cart (GET REQUEST)**

  http://localhost:8000/addtocart?id=xxxxxxx&userID=xxxxxxxxxxxx

- **Listing the item in the users cart (GET REQUEST) and total price**

  http://localhost:8000/listcart?id=xxxxxxuser_idxxxxxxxxxx

- **Addding the Address (POST REQUEST)**

  http://localhost:8000/addadress?id=user_id**\*\***\***\*\***

  The Address array is limited to two values home and work address more than two address is not acceptable

```json
{
  "house_name": "telkom house",
  "street_name": "telkom street",
  "city_name": "surabaya",
  "pin_code": "123456"
}
```

- **Editing the Home Address(PUT REQUEST)**

  http://localhost:8000/edithomeaddress?id=xxxxxxxxxxuser_idxxxxxxxxxxxxxxx

- **Editing the Work Address(PUT REQUEST)**

  http://localhost:8000/editworkaddress?id=xxxxxxxxxxuser_idxxxxxxxxxxxxxxx

- **Delete Addresses(GET REQUEST)**

  http://localhost:8000/deleteaddresses?id=xxxxxxxxxuser_idxxxxxxxxxxxxx

  delete both addresses

- **Cart Checkout Function and placing the order(GET REQUEST)**

  After placing the order the items have to be deleted from cart functonality added

  http://localhost:8000?id=xxuser_idxxx

- **Instantly Buying the Products(GET REQUEST)**
  http://localhost:8000?userid=xxuser_idxxx&pid=xxxxproduct_idxxxx
